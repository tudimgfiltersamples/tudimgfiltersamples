#include "image.h"
#include "optionparser.h"

#include <QDebug>
#include <QImage>
#include <QtGlobal>

template <typename T >
T sign( T val )
{
  if ( val < 0 )
  {
    return -1;
  } else if ( val > 0 )
  {
    return +1;
  }
  return 0;
}

bool absSort( const int &v1, const int &v2 )
{
  return qAbs( v1 ) < qAbs( v2 );
}

template < typename T >
int componentShock( T v1, T v2 )
{
  T diff = v2 - v1;
  T result = v1 - sign( diff ) * qAbs( diff );
  return qBound( 0, static_cast< int >( result ), 255 );
}

void shock( const QString &input,
                     const QString &output,
                     unsigned int iterations,
                     unsigned int radius )
{
  QImage in( input );
  QImage out( in );
  for ( unsigned int i = 0; i < iterations; i++ )
  {
    for ( int x = 0; x < in.width(); x++ )
    {
      QVector4D colorSum;
      for ( int y = 0; y < in.height(); y++ )
      {
        if ( y == 0 )
        {
          colorSum = Image::colorSum( in, x - radius, y,
                                          x + radius, y + radius );
        } else
        {
          QVector4D rem = Image::colorSum( in, x - radius, y - radius - 1,
                                               x + radius, y - radius - 1 ),
                    add = Image::colorSum( in, x - radius, y + radius,
                                               x + radius, y + radius );
          colorSum = colorSum - rem + add;
        }
        QColor current( in.pixel( x, y ) );
        QColor average = Image::averageColor( colorSum );
        QColor result( componentShock( current.red(), average.red() ),
                       componentShock( current.green(), average.green() ),
                       componentShock( current.blue(), average.blue() ) );
        out.setPixel( x, y, result.rgb() );
      }
    }
    in = out;
  }
  out.save( output );
}

int main( int argc, char **argv )
{
  OptionParser::Option
      input( "input", "i", "input", QVariant::String, false, QString(), true,
             "File to read input image from", "FILE" ),
      output( "output", "o", "output", QVariant::String, false, QString(), true,
              "File to write computed image to", "FILE" ),
      iterations( "iterations", "n", "iterations", QVariant::UInt, false, 1, false,
                  "Number of iterations to execute" ),
      radius( "radius", "r", "radius", QVariant::UInt, false, 1, false,
              "Radius of each pixel operation" );
  OptionParser parser;
  parser.addOption( input );
  parser.addOption( output );
  parser.addOption( iterations );
  parser.addOption( radius );
  if ( argc > 1 && parser.parse( argc, argv ) )
  {
    shock( parser.argument( input ),
           parser.argument( output ),
           parser.argument( iterations ).toUInt(),
          parser.argument( radius ).toUInt() );
  } else
  {
    qDebug();
    qDebug( "Usage:" );
    qDebug( "  %s [OPTIONS]", argc > 0 ? argv[ 0 ] : "shock" );
    qDebug();
    qDebug( "Options:" );
    parser.printHelp( "  " );
  }
  return 0;
}
