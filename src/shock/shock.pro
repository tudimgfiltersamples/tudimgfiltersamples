TEMPLATE = app
TARGET = shock
INCLUDEPATH += ../common
HEADERS =
SOURCES = main.cpp
debug {
    DESTDIR = ../../bin/Debug
} else {
    DESTDIR = ../../bin/Release
}
LIBS += -L${DESTDIR} -limagefiltercommons
