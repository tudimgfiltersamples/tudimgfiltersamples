#ifndef OPTIONPARSER_H
#define OPTIONPARSER_H

#include <QList>
#include <QObject>
#include <QVariant>

class OptionParser : public QObject
{

    Q_OBJECT

public:

  class Option
  {
  public:

    QString         name;
    QString         optShort;
    QString         optLong;
    QVariant::Type  type;
    bool            isArrayOpt;
    QVariant        defaultValue;
    bool            isMandatory;
    QString         argName;
    QString         description;

    Option( const QString &name = QString(),
            const QString &optShort = QString(),
            const QString &optLong = QString(),
            QVariant::Type type = QVariant::String,
            bool isArrayOpt = false,
            const QVariant &defaultValue = QVariant(),
            bool isMandatory = false,
            const QString &description = QString(),
            const QString &argName = QString() );
    bool isValid() const;

    QString niceArgumentName() const;

    bool operator ==( const Option &other ) const;
    bool operator !=( const Option &other ) const;

  };

  explicit OptionParser(QObject *parent = 0);
  virtual ~OptionParser();

  bool isAllowingExtraneousArguments() const;
  void setAllowExtraneousArguments( bool allow );

  const QStringList& extraneousArguments() const;
  QString argument( const Option &option ) const;
  QStringList arguments( const Option &option ) const;

  void addOption( Option option );

  bool parse( int argc, char ** argv );

  void printHelp( const QString &indentation = QString() ) const;

signals:

public slots:

private:

  class OptionParserPrivate;
  OptionParserPrivate *d;

};

uint qHash( const OptionParser::Option &option );

#endif // OPTIONPARSER_H
