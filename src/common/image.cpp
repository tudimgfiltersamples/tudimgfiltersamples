#include "image.h"

#include <QDebug>
#include <QtAlgorithms>
#include <QtGlobal>

/**
  @typedef PixelSet
  @brief Represents a (multi) set of pixels.

  The PixelSet describes a multi set of pixels. That means: It contains a list
  of color values and how often each value appears in the list.
  It does not store any positional information (i.e. from where a
  pixel has been taken from).

  This type can also be used as a simple set, as by conversion, a
  value is contained in the set, if and only if the value associated
  with the set is greater or equal to one. However, note, that
  provided operators take number each value appears in the list into
  account. Thus, when e.g. subtracting one pixel set from another,
  a value might still be in the resulting set, even when it appears on both
  sets (which is the case, when the it occurs more often in the first set which
  we subtract from it).
  */

/**
  @typedef ColorComparatorFnc
  @brief Function prototype for comparing color values

  This type defines a signature for functions that can be used to compare
  QColor values. The function should return true if and only if
  the first argument is less then or equal to the second argument.
  */

/**
  @brief Calculate union of two pixel sets

  Returns the union of the two input pixel multi sets
  */
PixelSet operator + ( const PixelSet &first, const PixelSet &second )
{
  PixelSet result;
  foreach ( QColor pixel, first.keys() )
  {
    result[ pixel ] = first[ pixel ];
  }
  foreach ( QColor pixel, second.keys() )
  {
    result[ pixel ] = result.value( pixel, 0 ) + second[ pixel ];
  }
  return result;
}

/**
  @brief Calculate difference of two pixel sets

  Returns the difference between two pixel multi sets.
  */
PixelSet operator - ( const PixelSet &first, const PixelSet &second )
{
  PixelSet result;
  foreach ( QColor pixel, first.keys() )
  {
    int v1 = first.value( pixel, 0 ),
        v2 = second.value( pixel, 0 );
    if ( v1 > v2 )
    {
      result[ pixel ] = v1 - v2;
    }
  }
  return result;
}

/**
  @brief Simple comparer for color values

  Provides very basic comparison of color values. Returns true,
  if the sum of all color channels of the first argument is less or equal
  to the sum of all color channels of the second one.
  */
bool simpleColorCompare( const QColor &first, const QColor &second )
{
  return ( first.red() + first.green() + first.blue() ) <=
         ( second.red() + second.green() + second.blue() );
}

/**
  @brief Calculate a hash value for a color.
  */
uint qHash( const QColor &color )
{
  return qHash( color.red() ) ^ qHash( color.green() ) ^ qHash( color.blue() );
}

/**
  @class Image
  @brief Provides static utility functions for image processing

  The Image class provides some static utility functions for
  working with images. For example, one can use it to query sums
  of ranges of pixels from an image and so forth. The methods are
  designed to be robust and work even with indices that point beyond
  the image boundaries. This means, algorithms get much simpler, as
  a lot of checks are made inside the Image class and the clients can
  concentrate on their unique work.

  */

/**
  @brief Returns summed color from image

  Returns the sum of colors from  a range of pixels from the image.
  The sum is returned via a 4 component vector, where the first component
  contains the summed red color channel, the second the green color channel and
  the third the blue color channel. The fourth component contains the
  number of pixels that actually are contained in the sum.

  @param image The image to read pixels from
  @param xmin First horizontal pixel to read
  @param ymin First vertical pixel to read
  @param xmax Last horizontal pixel to read
  @param ymax Last vertical pixel to read

  @returns A vector containing the summed colors per color channel.

  @note The method can deal with ranges that point outside the
  image boundaries. The actual number of pixels included in a sum
  is stored in the fourth vector component. Note, that this value can
  as well be zero, thus be careful when using this in division operations.
  */
QVector4D Image::colorSum( const QImage &image,
                          int xmin, int ymin, int xmax, int ymax )
{
  QVector4D result;
  for ( int x = xmin; x <= xmax; x++ )
  {
    for ( int y = ymin; y <= ymax; y++ )
    {
      if ( image.valid( x, y ) )
      {
        QColor color( image.pixel( x, y ) );
        result += QVector4D( color.red(), color.green(), color.blue(), 1 );
      }
    }
  }
  return result;
}

/**
  @brief Returns the average color from a color sum.
  */
QColor Image::averageColor( const QVector4D &colorSum )
{
  if ( colorSum.w() >= 0 )
  {
    return QColor( toColorRange( colorSum.x() / colorSum.w() ),
                   toColorRange( colorSum.y() / colorSum.w() ),
                   toColorRange( colorSum.z() / colorSum.w() ) );

  }
  return QColor();
}

/**
  @brief Convert an arbitrary value color range

  Converts an arbitrary value to an integer and makes sure, the returned
  value is in the range 0 and 255.
  */
template < typename T >
int Image::toColorRange( T value )
{
  int v = static_cast< int >( value );
  return qBound( 0, v, 255 );
}

/**
  @brief Returns the set of pixels in the specified range.

  Returns a multi set containing all the pixel values from the
  range specified by @p minx, @p miny, @p maxx and @p maxy from the
  @p image.
  */
PixelSet Image::pixels( const QImage &image,
                        int minx, int miny, int maxx, int maxy )
{
  PixelSet result;
  for ( int x = minx; x <= maxx; x++ )
  {
    for ( int y = miny; y <= maxy; y++ )
    {
      if ( image.valid( x, y ) )
      {
        QColor pixel( image.pixel( x, y) );
        result[ pixel ] = result.value( pixel, 0 ) + 1;
      }
    }
  }
  return result;
}

/**
  @brief Select the medium pixel color value

  Selects the medium pixel value from the pixel multi set. The @p pixels are
  sorted using the @p cmp function.
  */
QColor Image::pickMedium( const PixelSet &pixels, ColorComparatorFnc cmp )
{
  QList< QColor > pixelList;
  foreach ( QColor pixel, pixels.keys() )
  {
    for ( int i = 0; i < pixels.value( pixel, 0 ); i++ )
    {
      pixelList.append( pixel );
    }
  }
  qSort( pixelList.begin(), pixelList.end(), cmp );
  int mid = pixelList.size() / 2;
  if ( pixelList.size() > 0 )
  {
    return pixelList[ mid ];
  }
  return QColor();
}
