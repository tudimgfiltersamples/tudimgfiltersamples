TEMPLATE = lib
TARGET = imagefiltercommons
CONFIG += static
HEADERS = \
  optionparser.h \
    image.h
SOURCES = \
  optionparser.cpp \
    image.cpp
debug {
  DESTDIR = ../../bin/Debug
} else {
  DESTDIR = ../../bin/Release
}
VERSION = 0.0.0
