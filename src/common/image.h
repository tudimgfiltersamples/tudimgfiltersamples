#ifndef IMAGE_H
#define IMAGE_H

#include <QColor>
#include <QHash>
#include <QImage>
#include <QList>
#include <QObject>
#include <QVector4D>

typedef QHash< QColor, int > PixelSet;
typedef bool (*ColorComparatorFnc)( const QColor &first, const QColor &second );

PixelSet operator + ( const PixelSet &first, const PixelSet &second );
PixelSet operator - ( const PixelSet &first, const PixelSet &second );

bool simpleColorCompare( const QColor &first, const QColor &second );

uint qHash( const QColor &color );

class Image : public QObject
{

    Q_OBJECT

public:

  static QVector4D colorSum( const QImage &image,
                            int xmin, int ymin, int xmax, int ymax );
  static QColor averageColor( const QVector4D &colorSum );
  template< typename T >
  static int toColorRange( T value );
  static PixelSet pixels( const QImage &image,
                         int minx, int miny, int maxx, int maxy );
  static QColor pickMedium( const PixelSet &pixels,
                           ColorComparatorFnc cmp = simpleColorCompare );

signals:

public slots:

private:

  explicit Image(QObject *parent = 0) : QObject( parent ) {}
  Image( const Image & ) : QObject() {}

};

#endif // IMAGE_H
