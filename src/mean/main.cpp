#include "optionparser.h"

#include "image.h"

#include <QColor>
#include <QDebug>
#include <QImage>
#include <QVector4D>

void mean( const QString &input,
           const QString &output,
           unsigned int radius )
{
  QImage in( input );
  QImage out( in );

  for ( int x = 0; x < in.width(); x++ )
  {
    QVector4D colorSum;
    for ( int y = 0; y < in.height(); y++ )
    {
      if ( y == 0 )
      {
        // We have started a new column; reset sum
        colorSum = Image::colorSum( in, x - radius, y, x + radius, y + radius );
      } else
      {
        // We are making progress within the same column; just remove
        // color values that fall out of range by going one step further
        // and add the new ones
        colorSum = colorSum -
                   Image::colorSum( in, x - radius, y - radius - 1,
                                    x + radius, y - radius - 1 ) +
                   Image::colorSum( in, x - radius, y + radius,
                                    x + radius, y + radius );
      }
      out.setPixel( x, y, Image::averageColor( colorSum ).rgb() );
    }
  }
  out.save( output );
}

int main( int argc, char **argv )
{
  OptionParser::Option
      input( "input", "i", "input", QVariant::String, false, QString(), true,
             "Input image for the filter", "FILENAME" ),
      output( "output", "o", "output", QVariant::String, false, QString(), true,
              "File to save result image into", "FILENAME" ),
      radius( "radius", "r", "radius", QVariant::UInt, false, 0, false,
              "The blur radius" );
  OptionParser parser;

  parser.addOption( input );
  parser.addOption( output );
  parser.addOption( radius );

  if ( argc > 1 && parser.parse( argc, argv ) )
  {
    mean( parser.argument( input ),
          parser.argument( output ),
          parser.argument( radius ).toUInt() );
  } else
  {
    qDebug();
    qDebug( "Usage:" );
    qDebug( "  %s [OPTIONS]", argc > 0 ? argv[ 0 ] : "mean" );
    qDebug();
    qDebug( "Options:" );
    parser.printHelp( "  ");
  }
}
