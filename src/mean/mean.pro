TEMPLATE = app
TARGET = mean
INCLUDEPATH += ../common
HEADERS =
SOURCES = main.cpp
QT += gui
debug {
    DESTDIR = ../../bin/Debug
} else {
    DESTDIR = ../../bin/Release
}
LIBS += -L${DESTDIR} -limagefiltercommons
