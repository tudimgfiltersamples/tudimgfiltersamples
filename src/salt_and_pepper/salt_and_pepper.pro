TEMPLATE = app
TARGET = salt_and_pepper
INCLUDEPATH += ../common
HEADERS =
SOURCES = main.cpp
debug {
    DESTDIR = ../../bin/Debug
} else {
    DESTDIR = ../../bin/Release
}
LIBS += -L${DESTDIR} -limagefiltercommons
