#include "optionparser.h"

#include <QDebug>
#include <QImage>
#include <QTime>

void salt_and_pepper( const QString &input,
                     const QString &output,
                     unsigned int density )
{
  qsrand( QTime::currentTime().msec() );
  QImage img( input );
  int numPixels = img.width() * img.height() * density / 100;
  for ( int k = 0; k < numPixels; k++ )
  {
    int x = qrand() % img.width();
    int y = qrand() % img.height();
    img.setPixel( x, y, qRgb( qrand() % 256, qrand() %256, qrand() % 256 ) );
  }
  img.save( output );
}

int main( int argc, char **argv )
{
  OptionParser::Option
      input( "input", "i", "input", QVariant::String, false, QString(), true,
             "File to read input image from", "FILE" ),
      output( "output", "o", "output", QVariant::String, false, QString(), true,
              "File to write result image to", "FILE" ),
      density( "density", "d", "density", QVariant::UInt, false, 30, false,
               "Percentage of pixels to manipulate (should be in the range 0-100)" );
  OptionParser parser;
  parser.addOption( input );
  parser.addOption( output );
  parser.addOption( density );
  if ( argc > 1 && parser.parse( argc, argv ) )
  {
    salt_and_pepper( parser.argument( input ),
                     parser.argument( output ),
                     parser.argument( density ).toUInt() );
  } else
  {
    qDebug();
    qDebug( "Usage:" );
    qDebug( "  %s [OPTIONS]", argc > 0 ? argv[ 0 ] : "salt_and_pepper" );
    qDebug();
    qDebug( "Options:" );
    parser.printHelp( "  " );
  }
  return 0;
}
