#include "optionparser.h"

#include "image.h"

#include <QColor>
#include <QDebug>
#include <QImage>
#include <QList>

void median( const QString &input,
             const QString &output,
             unsigned int radius )
{
  QImage in( input );
  QImage out( in );
  for ( int x = 0; x < in.width(); x++ )
  {
    PixelSet pixels;
    for ( int y = 0; y < in.height(); y++ )
    {
      if ( y == 0 )
      {
        pixels = Image::pixels( in, x - radius, y - radius,
                                    x + radius, y + radius );
      } else
      {
        PixelSet remove = Image::pixels( in, x - radius, y - radius - 1,
                                             x + radius, y - radius - 1 );
        PixelSet add = Image::pixels( in, x - radius, y + radius,
                                          x + radius, y + radius );
        pixels = ( pixels - remove ) + add;
      }
      out.setPixel( x, y, Image::pickMedium( pixels ).rgb() );
    }
  }
  out.save( output );
}

int main( int argc, char **argv )
{
  OptionParser::Option
      input( "input", "i", "input", QVariant::String, false, QString(), true,
             "File to read input image from", "FILE" ),
      output( "output", "o", "output", QVariant::String, false, QString(), true,
              "File to write result picture into", "FILE" ),
      radius( "radius", "r", "radius", QVariant::UInt, false, 0, false,
              "Median radius" );
  OptionParser parser;

  parser.addOption( input );
  parser.addOption( output );
  parser.addOption( radius );

  if ( argc > 1 && parser.parse( argc, argv ) )
  {
    median( parser.argument( input ),
            parser.argument( output ),
            parser.argument( radius ).toUInt() );
  } else
  {
    qDebug();
    qDebug( "Usage:" );
    qDebug( "  %s [OPTIONS]", argc > 0 ? argv[ 0 ] : "median" );
    qDebug();
    qDebug( "Options:" );
    parser.printHelp( "  " );
  }

  return 0;
}
