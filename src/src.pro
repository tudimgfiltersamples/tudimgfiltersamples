TEMPLATE = subdirs
SUBDIRS = \
  common \
  mean \
  median \
  salt_and_pepper \
  shock
CONFIG += ORDERED
